AA Hearing Aid Center is a family-owned and operated business started by David Ogilvy’s father, Stephen H. Ogilvy Sr., over 70 years ago. Our growth and success is based on a “patient focused model,” matching the appropriate hearing aid technology to the specific needs of each individual patient.

Address: 111 High Ridge Rd, 3rd Floor, Stamford, CT 06905, USA

Phone: 203-348-2271

Website: https://aahearingstamfordct.com
